#!/usr/bin/env ruby
require_relative 'include'
require_relative 'delete_lib'

def database_menu
  loop do
    item = selection_menu(
        'Database menu',
        [:seed, :seed_single, :delete_orphans, :delete_finished_runs]
    )
    break unless item
    send("db_#{item}") if item.is_a?(Symbol)
    @options.clear
  end
end

def db_seed
  puts 'Seeding database ...'
  @initializer.seed_database
  puts 'Done.'
end

def db_seed_single
  file = select_path(true, true, @initializer.config.database.seed_dir)
  return if file.nil? || file.empty? || !File.file?(file) || !File.exist?(file)
  puts "Seeding #{File.basename(file)} ..."
  Libis::Ingester::Database::Seed.new([]).load_file(File.absolute_path(file))
end

def db_delete_orphans
  delete_orphan_items
end

def db_delete_finished_runs
  delete_all_finshed_runs
end
