require 'libis/ingester'
require 'libis/tools/extend/hash'

module Libis
  module Ingester
    class Database
      include ::Libis::Tools::Logger

      def initialize(cfg_file = nil, env = :production)
        ::Libis::Ingester.configure do |cfg|
          # noinspection RubyResolve
          cfg.database_connect((cfg_file || 'mongoid.yml'), env)
        end
      end

      def clear
        ::Libis::Ingester::Run.destroy_all rescue nil
        Mongoid.purge!
        self
      end

      def setup
        ::Libis::Ingester::AccessRight.create_indexes
        ::Libis::Ingester::IngestModel.create_indexes
        ::Libis::Ingester::Item.create_indexes
        ::Libis::Ingester::Job.create_indexes
        ::Libis::Ingester::Organization.create_indexes
        ::Libis::Ingester::DomainStorage.create_indexes
        ::Libis::Ingester::RepresentationInfo.create_indexes
        ::Libis::Ingester::RetentionPeriod.create_indexes
        ::Libis::Ingester::User.create_indexes
        ::Libis::Ingester::Workflow.create_indexes
        self
      end

      def seed(*args)
        # sources = [File.join(Libis::Ingester::ROOT_DIR, 'db', 'data')] + args
        Seed.new(args).load_data
        self
      end

      def self.find_by_name(object, name)
        return nil unless name
        klass = object if object.is_a?(Class)
        klass ||= "::Libis::Ingester::#{object.to_s.classify}".constantize
        klass.find_by(name: name) ||
            warn("Could not find %s '%s'" % [klass.to_s.split('::').last.underscore.humanize.downcase, name])
      end

      def self.find_or_create_by_name(object, name)
        return nil unless name
        klass = object if object.is_a?(Class)
        klass ||= "::Libis::Ingester::#{object.to_s.classify}".constantize
        klass.find_or_create_by(name: name)
      end

      class Seed

        attr_accessor :datadir, :config

        def initialize(sources)
          @datadir = []
          @config = {}
          sources.each do |source|
            case source
              when Hash
                @config.merge!(source.key_symbols_to_strings(recursive: true))
              when String
                raise RuntimeError, "'#{source}' not found." unless File.exist?(source)
                if File.directory?(source)
                  @datadir << source
                elsif File.file?(source)
                  cfg = read_yaml(source)
                  cfg = cfg.seed if cfg.seed
                  @config.merge(cfg)
                end
              else
                raise RuntimeError, 'Should supply a hash or file/directory name.'
            end
          end
        end

        DATA_TYPES=%w"organization user access_right retention_period ingest_model workflow job"

        # noinspection RubyResolve
        def load_data
          DATA_TYPES.each do |dt|
            puts "Loading #{dt}s ..."
            load_config(postfix: dt, klass: "Libis::Ingester::#{dt.classify}".constantize)
          end
        end

        def load_file(file)
          if file =~ /_(#{DATA_TYPES.join('|')}).cfg$/
            puts "Loading file #{File.basename(file)} ..."
            "Libis::Ingester::#{$1.classify}".constantize.from_hash(read_yaml(file))
          end
        end

        private

        def load_config(options = {})
          each_config(options[:postfix]) do |cfg, fname = 'site config'|
            puts " - #{cfg['name']} [#{fname}]" if cfg['name']
            yield(cfg) if block_given?
            options[:klass].from_hash(cfg)
          end
        end

        def each_config(postfix)
          case (cfg = @config[postfix])
            when Array
              cfg.map {|c| yield c}
            when Hash
              yield cfg
            else
              #skip
          end
          @datadir.each do |dir|
            Dir.glob("*_#{postfix}.cfg", base: dir).each do |filename|
              full_name = File.join(dir, filename)
              yield read_yaml(full_name), full_name
            end
          end
        end

        def read_yaml(file)
          config = Libis::Tools::ConfigFile.new({}, preserve_original_keys: false)
          config << file
          config.to_h.key_symbols_to_strings(recursive: true)
        rescue StandardError => e
          puts "ERROR while parsing file '#{file}'"
          raise e
        end

      end

    end
  end
end
