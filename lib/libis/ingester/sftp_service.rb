require 'net/ssh'
require 'net/sftp'
require 'stringio'

module Libis
  module Ingester
    class SftpService

      # Create FTP service
      # param [String] host
      # param [Integer] port
      # param [String] user
      # param [String] keyfile
      def initialize(_host, _port, _user, _keyfile = "#{File.join(ENV['HOME'], '.ssh', 'id_rsa')}")
        @host = _host
        @port = _port
        @user = _user
        @keyfile = _keyfile
        connect
      end

      # Get directory listing
      # param [String] dir
      # return [Array<String>]
      def ls(dir)
        check do
          ftp_service.dir.glob(dir, '*').entries.map { |e| e.name }
        end
      end

      # Download a file
      # param [String] remote_path remote file path
      # param [String] local_path
      # param [Symbol] mode :binary or :text
      def get_file(remote_path, local_path, _mode = :binary)
        check do
          ftp_service.download!(remote_path, local_path)
          true
        end
      end

      # Upload a file
      # param [String] remote_path remote file path
      # param [Array<String>] data
      # param [Symbol] mode :binary or :text
      def put_file(remote_path, data, _mode = :text)
        io = ::StringIO.new(data.join("\n"))
        check do
          ftp_service.upload!(io, remote_path)
          true
        end
      end

      # Delete a file
      # param [String] remote_path remote file path
      def del_file(remote_path)
        check do
          ftp_service.remove!(remote_path)
          true
        end
      end

      # Create a directory
      # param [String] remote_path remote directory path
      def create_dir(remote_path)
        check do
          ftp_service.mkdir!(remote_path)
          true
        end
      end

      # Delete a directory
      # param [String] remote_path remote directory
      def del_dir(remote_path)
        check do
          ftp_service.rmdir!(remote_path)
          true
        end
      end

      # Delete a directory
      # param [String] remote_path remote directory
      def del_tree(remote_path)
        ls(remote_path).map do |file|
          file = File.join(remote_path, file)
          is_file?(file) ? del_file(file) : del_tree(file)
        end
        del_dir(remote_path)
      end

      def exist?(remote_path)
        check do
          begin
            [:regular, :directory].include?(ftp_service.lstat!(remote_path).symbolic_type)
          rescue ::Net::SFTP::StatusException
            return false
          end
        end
      end

      # Check if remote path is a file (or a directory)
      # param [String] remote_path
      # return [Boolean] true if file, false if directory
      def is_file?(remote_path)
        check do
          ftp_service.lstat!(remote_path).file?
        end
      end

      protected
      
      attr_accessor :host, :port, :user, :keyfile

      # @return [Net:FTP]
      def ftp_service
        @ftp_service ||= Net::SFTP.start(
          host,
          user,
          port: port,
          keys: [keyfile],
          keys_only: true,
          timeout: 600,
          keepalive: true,
          keepalive_interval: 1800,
          verify_host_key: :accept_new
        )
      end

      CONNECTION_CONSTANTS = [
        Net::SFTP::Constants::StatusCodes::FX_NO_CONNECTION,
        Net::SFTP::Constants::StatusCodes::FX_CONNECTION_LOST
      ]

      # Tries to execute ftp commands; reconnects and tries again if connection timed out
      def check
        begin
          yield
        rescue Net::SFTP::StatusException => e
          return false unless CONNECTION_CONSTANTS.include?(e.code)
          reset_connection
          yield
        rescue Exception => e
          reset_connection
          begin
            yield
          rescue
            return false
          end
        end
      end

      # Connect to FTP server
      def connect
        ftp_service.connect!
      end

      # Disconnect from FTP server
      def disconnect
        ftp_service.session.shutdown!
      rescue
        # do nothing
      ensure
        @ftp_service = nil
      end

      def reset_connection
        begin
          disconnect
        rescue
        end
        connect
      end

    end
  end
end