require 'libis/ingester'
require 'fileutils'
require 'yaml'

module Libis
  module Ingester

    class SeedExporter

      attr_reader :export_dir

      def initialize(export_dir)
        FileUtils.mkdir_p(export_dir)
        raise RuntimeError, "'#{export_dir}' is not a directory" unless File.directory?(export_dir)
        raise RuntimeError, "'#{export_dir}' is not writable" unless File.writable?(export_dir)
        @export_dir = export_dir
      end

      def export_all
        export_access_rights
        export_retention_periods
        export_representation_infos
        export_organizations
      end

      def export_organizations
        Libis::Ingester::Organization.each { |org| export_organization(org) }
      end

      def export_access_rights
        Libis::Ingester::AccessRight.each { |ar| export_access_right(ar) }
      end

      def export_retention_periods
        Libis::Ingester::RetentionPeriod.each { |rp| export_retention_period(rp) }
      end

      def export_representation_infos
        Libis::Ingester::RepresentationInfo.each { |ri| export_representation_info(ri) }
      end

      def export_organization(org)
        raise RuntimeError, 'Invalid organization argument' unless org&.is_a?(Libis::Ingester::Organization)

        hash = org.to_hash

        # sanitize name
        name = sanitize(org.name)

        filename = File.join(export_dir, "#{name}_organization.cfg")
        File.open(filename, 'w') { |f| f.write(yamlize(hash)) }

        puts "Organization '#{org.name}' => #{filename}"

        org.jobs.each { |job| export_job(job) }
      end

      def export_access_right(ar)
        raise RuntimeError, 'Invalid access_right argument' unless ar&.is_a?(Libis::Ingester::AccessRight)

        hash = ar.to_hash

        # sanitize name
        name = sanitize(ar.name)

        filename = File.join(export_dir, "#{name}_access_right.cfg")
        File.open(filename, 'w') { |f| f.write(yamlize(hash)) }

        puts "AccessRight '#{ar.name}' => #{filename}"
      end

      def export_retention_period(rp)
        raise RuntimeError, 'Invalid retention_period argument' unless rp&.is_a?(Libis::Ingester::RetentionPeriod)

        hash = rp.to_hash

        # sanitize name
        name = sanitize(rp.name)

        filename = File.join(export_dir, "#{name}_retention_period.cfg")
        File.open(filename, 'w') { |f| f.write(yamlize(hash)) }

        puts "RetentionPeriod '#{rp.name}' => #{filename}"
      end

      def export_representation_info(ri)
        raise RuntimeError, 'Invalid representation_info argument' unless ri&.is_a?(Libis::Ingester::RepresentationInfo)

        hash = ri.to_hash

        # sanitize name
        name = sanitize(ri.name)

        filename = File.join(export_dir, "#{name}_representation_info.cfg")
        File.open(filename, 'w') { |f| f.write(yamlize(hash)) }

        puts "RepresentationInfo '#{ri.name}' => #{filename}"
      end

      def export_job(job)
        raise RuntimeError, 'Invalid job argument' unless job&.is_a?(Libis::Ingester::Job)

        hash = job.to_hash

        # remove _type
        hash.delete(:_type)
        # sanitize name
        name = sanitize(job.name)
        hash[:name] = name
        # get rid of default log entries
        hash.delete(:log_to_file) if hash[:log_to_file]
        hash.delete(:log_each_run) if hash[:log_each_run]
        hash.delete(:log_age) if hash[:log_age] == 'daily'
        hash.delete(:log_keep) if hash[:log_keep] == 5
        # replace organization link
        hash.delete(:organization_id)
        hash[:organization] = job.organization.name
        # replace workflow link
        hash.delete(:workflow_id)
        hash.delete(:workflow_type)
        hash[:workflow] = sanitize(job.workflow.name)
        # replace ingest model link
        hash.delete(:ingest_model_id)
        hash[:ingest_model] = sanitize(job.ingest_model.name)

        filename = File.join(export_dir, "#{name}_job.cfg")
        File.open(filename, 'w') { |f| f.write(yamlize(hash)) }

        puts "Job '#{job.name}' => #{filename}"

        export_workflow(job.workflow)
        export_ingest_model(job.ingest_model)
      end

      def export_workflow(wf)
        raise RuntimeError, 'Invalid workflow argument' unless wf&.is_a?(Libis::Ingester::Workflow)

        hash = wf.to_hash

        # remove _type
        hash.delete(:_type)
        # sanitize name
        name = sanitize(wf.name)
        hash[:name] = name
        # embed config
        cfg = hash.delete(:config)
        hash.merge!(cfg)

        filename = File.join(export_dir, "#{name}_workflow.cfg")
        File.open(filename, 'w') { |f| f.write(yamlize(hash)) }

        puts "Workflow '#{wf.name}' => #{filename}"
      end

      def export_ingest_model(im)
        raise RuntimeError, 'Invalid ingest model argument' unless im&.is_a?(Libis::Ingester::IngestModel)

        hash = im.to_hash

        # remove _type
        hash.delete(:_type)
        # sanitize name
        name = sanitize(im.name)
        hash[:name] = name
        # replace access right link
        hash.delete(:access_right_id)
        hash[:access_right] = im.access_right.name if im.access_right
        # process manifestations
        hash[:manifestations] = im.manifestations.map do |mf|
          mf_hash = mf.to_hash
          # replace access right link
          mf_hash.delete(:access_right_id)
          mf_hash[:access_right] = mf.access_right.name if mf.access_right
          # replace representation info link
          mf_hash.delete(:representation_info_id)
          mf_hash[:representation] = mf.representation_info.name if mf.representation_info
          # replace conversions
          mf_hash.delete(:convert_infos)
          mf_hash[:convert] = mf.convert_infos.map(&:to_hash)
          mf_hash
        end

        filename = File.join(export_dir, "#{name}_ingest_model.cfg")
        File.open(filename, 'w') { |f| f.write(yamlize(hash)) }

        puts "IngestModel '#{im.name}' => #{filename}"
      end

      def sanitize(name)
        name.gsub(/[\s_-]+/, '_')
      end

      def yamlize(hash)
        hash.key_symbols_to_strings(recursive: true).to_yaml
      end

    end
  end
end