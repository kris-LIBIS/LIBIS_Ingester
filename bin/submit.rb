#!/usr/bin/env ruby
require_relative '../lib/libis/ingester/console/submit_lib'
require 'awesome_print'

@initializer = ::Libis::Ingester::Initializer.init('site.config.yml')

@options = {}

option_parser = OptionParser.new do |opts|
  opts.banner = "Usage: #{$0} [options]"
  opts.separator ""
  opts.separator "  Job and queue options are required. If omitted, a list of valid values will be shown."
  opts.separator ""
  opts.separator "  options:"

  opts.on_tail('-r', '--run RUN_NAME', 'Run name')
  opts.on_tail('-e', '--email ADDRESS', 'Email address for status reporting')
  opts.on_tail('-v', '--verbose')
  opts.on_tail('-h', '--help', 'Prints this help') do
    puts opts
    exit
  end

end

option_parser.accept(Libis::Ingester::Job) do |name|
  job = Libis::Ingester::Job.find_by(name: name) || Libis::Ingester::Job.find_by(description: name)
  raise Exception.new "Job with name '#{name}' not found" unless job
  job
end

option_parser.on('-j', '--job JOB_NAME', Libis::Ingester::Job, "Select a job by its name")

option_parser.accept(Sidekiq::Queue) do |name|
  not_found = -> { raise "Queue with name '#{name}' not found"}
  Sidekiq::Queue.all.find(not_found) { |queue| queue.name == name }
end

option_parser.on('-q', '--queue QUEUE_NAME', Sidekiq::Queue, "Select a queue by its name")

option_parser.accept(Hash) do |options|
  result = {}
  options.split(';').each do |option|
    key, value = option.split('=')
    result[key] = value if key
  end
  result
end

option_parser.on('-o', '--options OPTIONS', Hash, 'Options as key=value[,key=value[...]]', "'-o ?' for a list of options and default values" )

option_parser.parse!(into: @options)

unless Libis::Ingester::Job === @options[:job]
  puts "Job names:"
  Libis::Ingester::Job.pluck(:name).each {|name| puts " - #{name}"}
  exit
end

@options[:args] = {}

@options[:job].workflow.config['input'].each do |key, value|
  @options[:args][key] = value['default']
end if @options[:job].workflow.config['input']

@options[:job].input.each do |key, value|
  @options[:args][key] = value
end if @options[:job].input

if @options[:options]&.has_key?('?')
  puts "Available options for job #{@options[:job].name}:"
  @options[:args].each {|key, value| puts " - #{key} = #{value}"}
  exit
end

@options[:options]&.each { |key, value| @options[:args][key] = value }

@options[:args]['run_name'] = clean_string(@options[:run]) if @options[:run]

@options[:args]['run_config'] = { 'error_to' => @options[:email], 'success_to' => @options[:email]} if @options[:email]

unless Sidekiq::Queue === @options[:queue]
  puts "Queue names:"
  Sidekiq::Queue.all.map(&:name).each {|name| puts " - #{name}"}
  exit
end

Sidekiq::Client.push(
  'class' => 'Libis::Ingester::JobWorker',
  'queue' => @options[:queue].name,
  'retry' => false,
  'args' => [ @options[:job].id.to_s, @options[:args] ]
)

if @options[:verbose]
  puts "Run for job #{@options[:job].name} submitted with arguments:"
  @options[:args].each {|key, value| puts " - #{key} = #{value}"}
end
