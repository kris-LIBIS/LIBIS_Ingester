# Build arguments
ARG USER_NAME=ingester
ARG GROUP_NAME=ingester
ARG UID=1000
ARG GID=1000
ARG APP_DIR=/ingester
ARG TZ=Europe/Brussels

# Build stage image
FROM registry.docker.libis.be/teneo/ruby-base:latest as build

# Create application user
ARG USER_NAME
ARG GROUP_NAME
ARG UID
ARG GID
ARG APP_DIR
RUN groupadd --gid ${GID} ${GROUP_NAME} \
 && useradd --home-dir /home/ingester --create-home --no-log-init --uid ${UID} --gid ${GID} ${USER_NAME} \
 && mkdir ${APP_DIR} \
 && chown ${USER_NAME}:${GROUP_NAME} ${APP_DIR}

USER ${USER_NAME}
WORKDIR ${APP_DIR}

# Copy the source files
ADD distribution.tar .

# Make Oracle instantclient accessable
ENV LD_LIBRARY_PATH=${APP_DIR}/instantclient

# Install gems
RUN bundle config set --local clean 'true' \
 && bundle config set --local deployment 'true' \
 && bundle config set --local frozen 'true' \
 && bundle config set --local disable_version_check 'true' \
 && bundle config set --local path 'vendor/bundle' \
 && bundle config set --local without development:test \
 && bundle install --no-cache

# Final image
FROM registry.docker.libis.be/teneo/ruby-base

# Overwrite Entrypoint script

RUN apt-get update -qq \
    && apt-get -qqy upgrade \
    && apt-get install -qqy --no-install-recommends \
      ksh \
      libchromaprint-dev \
      ffmpeg \
      libreoffice \
      imagemagick \
      ghostscript \
      gsfonts \
      fonts-liberation \
      clamav clamav-freshclam \
      python-2.7 python-pip python-setuptools python-wheel \
      unzip \
      default-jre \
      apt-transport-https software-properties-common \
    && wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add - \
    && add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/ \
    && apt-get update -qq \
    && apt-get install -qqy --no-install-recommends adoptopenjdk-8-hotspot \
    && apt-get clean \
    && rm -fr /var/cache/apt/archives/* \
    && rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp* \
    && truncate -s 0 /var/log/*log

# Select java version
ENV JAVA_HOME=/usr/lib/jvm/adoptopenjdk-8-hotspot-amd64
RUN update-alternatives --set java ${JAVA_HOME}/bin/java

# Install fido
RUN pip install opf-fido

# Install droid
RUN wget -q https://github.com/digital-preservation/droid/releases/download/droid-6.5/droid-binary-6.5-bin.zip \
    && unzip -qd /opt/droid droid-binary-6.5-bin.zip \
    && chmod 755 /opt/droid/droid.sh \
    && sed -i -e 's/^OPTIONS=""/OPTIONS="-Dlog4j2.formatMsgNoLookups=true"ui/g' /opt/droid/droid.sh \
    && rm droid-binary-6.5-bin.zip

# Set timezone
ARG TZ
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Create application user
ARG USER_NAME
ARG GROUP_NAME
ARG UID
ARG GID
ARG APP_DIR
RUN groupadd --gid ${GID} ${GROUP_NAME} \
 && useradd --home-dir /home/ingester --create-home --no-log-init --uid ${UID} --gid ${GID} ${USER_NAME} \
 && mkdir ${APP_DIR} \
 && chown ${USER_NAME}:${GROUP_NAME} ${APP_DIR}

# Switch to application user 
USER ${USER_NAME}
WORKDIR ${APP_DIR}

ENV NLS_LANG=AMERICAN_AMERICA.AL32UTF8
ENV PATH=/oracle-client:${PATH}

# Copy files into image
COPY --from=build ${APP_DIR} ${APP_DIR}

# Make Oracle instantclient accessable
ENV LD_LIBRARY_PATH=${APP_DIR}/instantclient

# Configure bundler
RUN bundle config set --local clean 'true' \
 && bundle config set --local deployment 'true' \
 && bundle config set --local frozen 'true' \
 && bundle config set --local disable_version_check 'true' \
 && bundle config set --local path 'vendor/bundle' \
 && bundle config set --local without development:test

# Start the menu
CMD ["ruby", "bin/main_menu.rb", "--config", "site.config.yml"]
