include .env
-include .env.local
export

UID ?= $(shell id -u)
GID ?= $(shell id -g)

.SILENT:

.PHONY: build push

all: collect build push

collect:
	tar -cf distribution.tar --owner=$(UID) --group=$(GID) \
		--exclude=lib/libis/ingester/server \
		bin config lib instantclient Gemfile Gemfile.lock Rakefile
build:
	docker build -t $(IMAGE_TAG) --build-arg UID=$(UID) --build-arg GID=$(GID) --build-arg USER_NAME=$(USER_NAME) --build-arg GROUP_NAME=$(GROUP_NAME) .

push:
	docker push $(IMAGE_TAG)
